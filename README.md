Robert Scott - Software Engineer
Work Samples

Available online on AWS:   http://ec2-52-8-224-34.us-west-1.compute.amazonaws.com/TicTacToe.html

Restful Web Service: Online Tic-Tac-Toe

Online Tic-Tac-Toe is a restful web service implemented using HTML and JavaScript on the client and a Java Servlet for the server.

The client sends the game state as JSON with an HTTP Get query parameter.

The server does a full recursive analysis and returns the server move with the HTTP response.


Robert Scott

email:		robscott57@gmail.com

linkedin:	https://www.linkedin.com/in/robscott57/